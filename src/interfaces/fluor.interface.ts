import {Salt} from './salt.interface';
import {Water} from './water.interface';

export interface Flour {
  getWater(): Water;
  getSalt(): Salt;
}
