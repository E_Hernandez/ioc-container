import {injectable} from 'inversify';
import {Water} from '../interfaces/water.interface';

@injectable()
export class WaterEntity implements Water {
  water!: string;
  public constructor() {
    console.log('Water class is being created');
  }
}
