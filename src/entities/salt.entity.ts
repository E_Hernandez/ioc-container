import {injectable} from 'inversify';
import {Salt} from '../interfaces/salt.interface';

@injectable()
export class SaltEntity implements Salt {
  salt!: string;
  public constructor() {
    console.log('Salt class is being created');
  }
}
