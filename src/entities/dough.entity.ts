import {inject, injectable} from 'inversify';
import {Dough} from '../interfaces/dough.interface';
import {Flour} from '../interfaces/fluor.interface';
import {Yeast} from '../interfaces/yeast.interface';
import {TYPES} from '../types';

@injectable()
export class DoughEntity implements Dough {
  public flour: Flour;
  public yeast: Yeast;

  public constructor(
    @inject(TYPES.Flour) flour: Flour,
    @inject(TYPES.Yeast) yeast: Yeast
  ) {
    console.log('Dough class is being created');
    this.flour = flour;
    this.yeast = yeast;
  }

  public getFlour(): Flour {
    return this.flour;
  }
  public getYeast(): Yeast {
    return this.yeast;
  }
}
