import {inject, injectable} from 'inversify';
import {Flour} from '../interfaces/fluor.interface';
import {Salt} from '../interfaces/salt.interface';
import {Water} from '../interfaces/water.interface';
import {TYPES} from '../types';

@injectable()
export class FlourEntity implements Flour {
  public water: Water;
  public salt: Salt;

  public constructor(
    @inject(TYPES.Watter) water: Water,
    @inject(TYPES.Salt) salt: Salt
  ) {
    console.log('Flour class is being created');
    this.water = water;
    this.salt = salt;
  }

  public getWater(): Water {
    return this.water;
  }
  public getSalt(): Salt {
    return this.salt;
  }
}
