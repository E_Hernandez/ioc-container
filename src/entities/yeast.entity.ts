import {inject, injectable} from 'inversify';
import {Yeast} from '../interfaces/yeast.interface';
import {TYPES} from '../types';

@injectable()
export class YeastEntity implements Yeast {
  yeast!: string;
}
