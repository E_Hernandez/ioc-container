import {inject, injectable} from 'inversify';
import 'reflect-metadata';
import {Dough} from './interfaces/dough.interface';
import {TYPES} from './types';

@injectable()
export class Pizza {
  public dough;
  public constructor(@inject(TYPES.Dough) dough: Dough) {
    console.log('Pizza class is being created');
    this.dough = dough;
  }
}
