const TYPES = {
  Dough: Symbol.for('Dough'),
  Flour: Symbol.for('Flour'),
  Yeast: Symbol.for('Yeast'),
  Salt: Symbol.for('Salt'),
  Watter: Symbol.for('Watter'),
};

export {TYPES};
