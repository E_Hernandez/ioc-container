import {Container} from 'inversify';
import {DoughEntity} from '../src/entities/dough.entity';
import {FlourEntity} from '../src/entities/flour.entity';
import {Dough} from './interfaces/dough.interface';
import {Flour} from './interfaces/fluor.interface';
import {Water} from './interfaces/water.interface';
import {Salt} from './interfaces/salt.interface';
import {SaltEntity} from '../src/entities/salt.entity';
import {TYPES} from './types';
import {WaterEntity} from '../src/entities/water.entity';
import {Yeast} from './interfaces/yeast.interface';
import {YeastEntity} from '../src/entities/yeast.entity';

const pizzaContainer = new Container();

pizzaContainer.bind<Dough>(TYPES.Dough).to(DoughEntity);
pizzaContainer.bind<Flour>(TYPES.Flour).to(FlourEntity);
pizzaContainer.bind<Water>(TYPES.Watter).to(WaterEntity);
pizzaContainer.bind<Salt>(TYPES.Salt).to(SaltEntity);
pizzaContainer.bind<Yeast>(TYPES.Yeast).to(YeastEntity);

export {pizzaContainer};
