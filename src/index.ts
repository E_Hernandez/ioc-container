import {Pizza} from './pizza';
import {pizzaContainer} from './pizza.container';

const pizza: Pizza = pizzaContainer.resolve<Pizza>(Pizza);
