"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TYPES = void 0;
const TYPES = {
    Dough: Symbol.for('Dough'),
    Flour: Symbol.for('Flour'),
    Yeast: Symbol.for('Yeast'),
    Salt: Symbol.for('Salt'),
    Watter: Symbol.for('Watter'),
};
exports.TYPES = TYPES;
//# sourceMappingURL=types.js.map