import { Flour } from './fluor.interface';
import { Yeast } from './yeast.interface';
export interface Dough {
    getFlour(): Flour;
    getYeast(): Yeast;
}
