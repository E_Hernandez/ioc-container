import { Dough } from './interfaces/dough.interface';
import { Flour } from './interfaces/fluor.interface';
import { Yeast } from './interfaces/yeast.interface';
export declare class DoughEntity implements Dough {
    flour: Flour;
    yeast: Yeast;
    constructor(flour: Flour, yeast: Yeast);
    getFlour(): Flour;
    getYeast(): Yeast;
}
