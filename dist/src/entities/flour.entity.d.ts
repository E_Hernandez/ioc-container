import { Flour } from '../interfaces/fluor.interface';
import { Salt } from '../interfaces/salt.interface';
import { Water } from '../interfaces/water.interface';
export declare class FlourEntity implements Flour {
    water: Water;
    salt: Salt;
    constructor(water: Water, salt: Salt);
    getWater(): Water;
    getSalt(): Salt;
}
