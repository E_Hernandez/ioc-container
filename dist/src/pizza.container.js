"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pizzaContainer = void 0;
const inversify_1 = require("inversify");
const dough_entity_1 = require("../src/entities/dough.entity");
const flour_entity_1 = require("../src/entities/flour.entity");
const salt_entity_1 = require("../src/entities/salt.entity");
const types_1 = require("./types");
const water_entity_1 = require("../src/entities/water.entity");
const yeast_entity_1 = require("../src/entities/yeast.entity");
const pizzaContainer = new inversify_1.Container();
exports.pizzaContainer = pizzaContainer;
pizzaContainer.bind(types_1.TYPES.Dough).to(dough_entity_1.DoughEntity);
pizzaContainer.bind(types_1.TYPES.Flour).to(flour_entity_1.FlourEntity);
pizzaContainer.bind(types_1.TYPES.Watter).to(water_entity_1.WaterEntity);
pizzaContainer.bind(types_1.TYPES.Salt).to(salt_entity_1.SaltEntity);
pizzaContainer.bind(types_1.TYPES.Yeast).to(yeast_entity_1.YeastEntity);
//# sourceMappingURL=pizza.container.js.map