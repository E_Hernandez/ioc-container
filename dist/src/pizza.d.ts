import 'reflect-metadata';
import { Dough } from './interfaces/dough.interface';
export declare class Pizza {
    dough: Dough;
    constructor(dough: Dough);
}
